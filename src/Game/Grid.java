package Game;

import Player.Player;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Grid {
    private  int x;
    private  int y;
    private int width;
    private int height;
    private int col;
    private  int row;
    private int cellSize =60;
    Rectangle rectangle;
    public static final int PADDING = 10;
    Picture background = new Picture(PADDING,PADDING,"resources/grassBackground.jpg");
    //CONSTRUCTOR
    public Grid(int width, int height) {
        this.width= width;
        this.height = height;
    }


    //GETTERS AND SETTERS
    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getRow() {
        return row;
    }

    public int getCol(){
        return col;
    }

    //ROW AND COLS TO X
    public int rowToY(int row) {
        return PADDING + row * cellSize;
    }

    public int columnToX(int column) {
        return PADDING + column * cellSize;
    }
    //STAR GRID
    public void init (){
        rectangle = new Rectangle( PADDING, PADDING,width,height);
         rectangle.draw();
        rectangle.setColor(Color.BLACK);
       // background.draw();
        makePlayer();
    }
    public void makePlayer(){
        Picture picturePlayer = new Picture(((width-37)/2),((height-45)/2),"resources/tower.png");
        Player player = new Player(picturePlayer);
        player.draw();
    }
    /// GRID BACKGROUND










}
