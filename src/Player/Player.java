package Player;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player {

    private Picture player;
    private int health;
    private int damage;

    public Player(Picture player){
        this.player = player;

    }


    public int getDamage() {
        return damage;
    }
    public int getHealth() {
        return health;
    }
    // metodo health e damage (UPGRADES)
    public void setDamage(int damage) {
        this.damage = damage;
    }
    public void setHealth(int health) {
        this.health = health;
    }

    public void draw(){
      player.draw();
    }

    public void hit ( ){

    }

    public void healthLost(){}

}

